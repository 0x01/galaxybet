import { fork } from 'redux-saga/effects';
import socketSaga from './../common/sagas/socket.saga';
import authSaga from './../common/sagas/auth.saga';
import apiSaga from './../common/sagas/api.sagas';
import betSaga from './../common/sagas/bet.saga';

const sagas = [
  ...authSaga,
  ...apiSaga,
  ...socketSaga,
  ...betSaga
];

export default function* rootSaga() {
  yield sagas.map(saga => fork(saga)); 
}
