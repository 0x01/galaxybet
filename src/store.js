import { compose, createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import rootReducer from '../common/reducers';
// import saga from '../common/sagas';
import saga from './sagas.js';
import persistState, {mergePersistedState} from 'redux-localstorage';
import adapter from 'redux-localstorage/lib/adapters/localStorage';
import filter from 'redux-localstorage-filter';
import _ from 'lodash';

export default function configureStore() {
  const sagaMiddleware = createSagaMiddleware();
  const reducer = compose(
    mergePersistedState((initialState, persistedState) => {
      console.log('initialState', initialState)
      console.log('rersistedState', persistedState)
      console.log(_.merge({}, initialState, persistedState));
      return _.merge({}, initialState, persistedState);
    }),
  )(rootReducer);

  const storage = compose(
    filter('app')
  )(adapter(window.localStorage));

  const enhancer = compose(
    applyMiddleware(
      sagaMiddleware,
      logger()
    ),
    // TODO change state for something more unique like appName:state
    persistState(storage, 'state')
  );
  const store = createStore( reducer, enhancer );

  sagaMiddleware.run(saga);
  console.log('localstorage', localStorage)
  return store;
}
