import React, { PropTypes } from 'react';
import { connect }          from 'react-redux';
import { Toaster, Position, Intent } from '@blueprintjs/core';
import Layout               from '../../components/Layout';
import BalanceBox           from '../../components/BalanceBox';
import BetBox               from '../../components/BetBox';
import BetsTable            from '../../components/BetsTable';
import { title, html }      from './index.md';
import { clearToast }       from '../../common/actions';
import '../styles/main.styl';
import './styles.styl';

@connect(state => ({
  toastQueue: state.chat.toastQueue, // State
}), { clearToast })      // Actions

export default class HomePage extends React.Component {
  static propTypes = {
    articles: PropTypes.array.isRequired,
    toastQueue: PropTypes.array.isRequired,
  };

  componentDidMount() {
    document.title = title;
  }

  componentDidUpdate() {
    if (this.props.toastQueue.length > 0) {
      this.props.toastQueue.map((message) => {
        console.log('message', ...message);
        this.toaster.show({
          iconName: "warning-sign",
          intent: Intent.WARNING,
          ...message
        });
      });
      this.props.clearToast();
    }
  }
  render() {
    return (
      <div>
        <Toaster
          position={Position.TOP_CENTER}
          canEscapeKeyClear={true}
          ref={(el) => this.toaster = el} />
        <Layout className="content">
          <BalanceBox />
          <BetBox />
          <BetsTable />
        </Layout>
      </div>
    );
  }

}

/* { this.props.children }*/


/* <div dangerouslySetInnerHTML={{ __html: html }} />
 * <h4>Articles</h4>
 * <ul>
 *   {this.props.articles.map((article, i) =>
 *     <li key={i}><a href={article.url}>{article.title}</a> by {article.author}</li>
 *    )}
 * </ul>
 * <p>
 *   <br /><br />
 * </p>*/
