---
title: React Static Boilerplate
---

## Welcome!

This is a single-page application powered by React and Material Design Lite (MDL).
When subscribed to "DEPOSITS"

Subscribe to "DEPOSITS" to receive push notifications whenever changes are detected in the unconfirmed and confirmed balances of the user for this app.

These notifications allow you to provide instant feedback whenever the user deposits bitcoin into your app via the blockchain.

Reminder: Moneypot waits for one blockchain confirmation before it lets a user spend bitcoin.

Within seconds of the user making a deposit to a deposit-address for your app, you will receive an unconfirmed_balance_change event so that you can immediately reassure the user that their bitcoin was successfully received but awaiting confirmation before it can be spent.

When a user's deposit makes it into a block on the blockchain, socketpot will send a unconfirmed_balance_change event and a (confirmed) balance_change event that enable you to update your UI appropriately without requiring the user to refresh the page.


https://github.com/kriasoft/react-static-boilerplate

