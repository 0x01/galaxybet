---
title: FAQ
---

## What is TwiceDice?
TwiceDice is Bitcoin based online gambling site established in 2017. 
We allow users from worldwide to play games with cryptocurrency at our website. 

## WHAT IS THE HOUSE EDGE?

The house edge when rolling on TwiceDice is only 1% (RTP 99%).

This industry low edge provides users a reasonable opportunity to make money unlike 
real money casinos which feature edges as high as 15% or more. 
Due to credit card and wire fees along with fraud expenses “real money” casinos simply cannot afford to offer an edge this low.
We’re able to pass these savings to our players and offer the smartest possible way to gamble.

## Why should I trust you?
We have partnered with Moneypot.com, utilizing their proven technology platform to bring our players secure and provably fair Bitcoin gambling. 
Using their wallet technology allows you to have instant trust in Betterbets.io and the ability to invest in all our games via their Kelly investment option.
The partnership makes sense on many levels and they are a pleasure to work with as well.
