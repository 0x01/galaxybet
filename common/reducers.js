import { combineReducers } from 'redux';
import { createReducer } from 'redux-act';
import { genUuid } from './helpers';
import config from './mpConfig';
import {
  initChat, login, userData, logout, newToken, allBets, depositAddress, nextHash,
  userAuthenticated, toggleChat, newMessage, changeChannel, showToast, clearToast,
  bankrollData, updateWager, updateMultiplier, updateCondition, updateTarget,
  betSuccess
} from './actions';

const initial = {
  app: {
    isAuthenticated: false,
    access_token: undefined,
    expires_at: undefined,
    hotkeysEnabled: false,
    deposit_address: undefined,
    user: {
      auth: {
        user: {}
      }
    },
    bankroll: {}
  },
  chat: {
    username: null,
    role: null,
    auth_id: null,
    channel: 'lobby',
    isOpen: false,
    messages: [],
    userlist: {},
    toastQueue: []
  },
  bets: {
    hash: undefined,
    all_bets: [],
    wager: new Number(0.00000000),
    multiplier: undefined,
    condition: undefined,
    target: undefined,
    latest_bet: undefined
  }
};

const app = createReducer({
  [newToken]: (state, payload) => {
    return {
      ...state,
      ...payload
    };
  },
  [userAuthenticated]: (state, payload) => {
    return {
      ...state,
      isAuthenticated: payload
    };
  },
  [userData]: (state, payload) => {
    return {
      ...state,
      user: {...payload}
    };
  },
  [bankrollData]: (state, payload) => {
    return {
      ...state,
      bankroll: {...payload}
    };
  },
  [depositAddress]: (state, payload) => {
    return {
      ...state,
     ...payload
    };
  },
  [logout]: (state, payload) => {
    return {
      ...state,
      isAuthenticated: false,
      // access_token: undefined,
      // expires_at: undefined,
      user: {
        auth: {
          user: {}
        }
      }
      
    };
  }
}, initial.app);

const chat = createReducer({
  [initChat]: (state, payload) => {
    console.log('payload app', payload);
    console.log('state app', state);
    return {
      ...state,
      username: payload.user.uname,
      role: payload.user.role,
      auth_id: payload.user.auth_id,
      userlist: {...payload.userlist},
      messages: [...payload.messages.map(msg => {
        msg.id = genUuid();
        return msg;
      })]
    };
  },
  [toggleChat]: (state, payload) => {
    return {
      ...state,
      isOpen: payload
    };
  },
  [newMessage]: (state, payload) => {
    return {
      ...state,
      messages: [...state.messages, {...payload, id: genUuid()}]
    };
  },
  [changeChannel]: (state, payload) => {
    return {
      ...state,
      ...payload
    };
  },
  [showToast]: (state, payload) => {
    return {
      ...state,
      toastQueue: [...state.toastQueue, { ...payload }]
    };
  },
  [clearToast]: (state, payload) => {
    return {
      ...state,
      toastQueue: []
    };
  }
}, initial.chat);


const bets = createReducer({
  [allBets]: (state, payload) => {
    return { ...state, all_bets: [...payload] };
  },
  [nextHash]: (state, payload) => {
    return { ...state, ...payload};
  },
  [updateWager]: (state, payload) => {
    return { ...state, wager: payload};
  },
  [updateMultiplier]: (state, payload) => {
    return { ...state, multiplier: payload};
  },
  [updateCondition]: (state, payload) => {
    return {...state, condition: payload};
  },
  [updateTarget]: (state, payload) => {
    return {...state, target: payload};
  },
  [betSuccess]: (state, payload) => {
    return {...state, latest_bet: payload};
  }
}, initial.bets);

export default combineReducers(
  { app, chat, bets }
);
