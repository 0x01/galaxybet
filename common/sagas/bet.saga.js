import { fork, take, select, race, call, put, cancel, takeEvery, actionChannel } from 'redux-saga/effects';
import {
  newToken, userData, bankrollData, logout, login, loginRequest, allBets, nextHash,
  userAuthenticated, initChat, newMessage, sendMessage, depositAddress,
  showToast, placeBet, betSuccess
} from '../actions';

function* betFlow() {
  const bet = yield takeEvery(placeBet);
  while(bet) {
    const bets = yield select(getBets);
    // console.log('bets from betflow', bets.wager);
    const wager = parseInt(bets.wager * 100000000);
    console.log('wager from betflow', wager);
    const profit = wager * (bets.multiplier - 1);
    console.log('profit from betflow', profit);
    const bodyParams = {
      wager: wager,
      client_seed: 0,
      hash: bets.bet_hash,
      cond: bets.condition,
      target: bets.target,
      payout: profit
    };
    const {response} = yield call(api.simpleDice, bodyParams);
    console.log('resporse from betflow', response);
    yield put(betSuccess(response));
  }
}

export default [betFlow];
