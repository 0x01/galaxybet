import { fork, take, call, put, actionChannel } from 'redux-saga/effects';
import * as api from '../sagas/api';

import {
  userData, bankrollData, allBets, nextHash, newHash,
  userAuthenticated, depositAddress
} from '../actions';

// ==== another saga for api ====
function* apiFlow() {
  yield call(fetchAllBets);
  const isAuth = yield actionChannel(userAuthenticated);
  // while (isAuth) {
    // TODO change to another action?
    // yield take(userAuthenticated);
    yield [
      call(fetchToken),
      call(fetchBankroll),
      call(fetchDepositAddress),
      call(postHashes)
    ];
  // }
}

function* fetchBankroll() {
  const {response} = yield call(api.fetchBankroll);
  yield put(bankrollData(response));
}

function* fetchToken() {
  const {response} = yield call(api.fetchToken);
  yield put(userData(response));
}

function* fetchDepositAddress() {
  const {response} = yield call(api.fetchAdress);
  yield put(depositAddress(response));
}

function* fetchAllBets() {
  const {response} = yield call(api.fetchAllBets);
  yield put(allBets(response));
}

// function* fetchHashes() {
//   const {response} = yield call(api.postHashes);
//   yield put(newHash(response));
// }

function* postHashes() {
  const {response} = yield call(api.postHashes);
  yield put(nextHash(response));
}

export default [apiFlow];
