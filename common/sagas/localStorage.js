export const getAuthToken = () => {
  console.log('auth token returned');
  return JSON.parse(localStorage.getItem('authToken')); 
};

export const setAuthToken = (token) => {
  console.log('auth token saved');
  localStorage.setItem('authToken', JSON.stringify(token));
};

export const removeAuthToken = () => {
  localStorage.removeItem('authToken');
};

export const clearLocalStorage = () => {
  localStorage.clear();
};
