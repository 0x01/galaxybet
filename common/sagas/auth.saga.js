import { fork, take, select, race, call, put, cancel, takeEvery, takeLatest } from 'redux-saga/effects';
import {
  newToken, userData, bankrollData, logout, login, loginRequest, allBets, nextHash,
  userAuthenticated, initChat, newMessage, sendMessage, depositAddress,
  showToast, placeBet, betSuccess
} from '../actions';
import { Intent } from "@blueprintjs/core";

// import { getToken } from './selectors';
import { setAuthToken, getAuthToken, clearLocalStorage } from './localStorage';
import * as helpers from './../helpers';


function* authorize(tokenVal) {
  // take token
  const token = tokenVal.access_token;
  if (token) {
    yield put(userAuthenticated(true));
    return true;
  } 
  // user signed out before server response OR server responded first but with error
  else {
    yield call(logout);
    yield call(clearLocalStorage);
    // TODO toaster here
    return null;
  }
}

function* authFlow() {
  let token = yield call(getAuthToken); // retreive from local storage
  // TODO need to add expiration validation
  while(true) {
    if(!token) {
      const payload = yield takeEvery(newToken);
      yield call(authorize, payload);
      token = payload.access_token;
    }

    yield put(userAuthenticated(true));
    // TODO watch token expired
    let userSignedOut;
    while( !userSignedOut ) {
      // console.log('waiting for expired', userSignedOut)
      const {expired} = yield race({
        // expired : wait(token.expires_in),
        signout : take(logout)
      });

      console.log('expired', expired);
      // token expired first
      if(expired) {
        token = yield call(authorize, token);
        // authorization failed, either by the server or the user signout
        if(!token) {
          userSignedOut = true; // breaks the loop
          yield put(logout);
        }
      } 
      // user signed out before token expiration
      else {
        userSignedOut = true; // breaks the loop
        yield put(logout);
      }
    }
  }
}

// function* watchTokenExpired() {
//   const expiresAt = yield select(getExpiresAt);
//   const tokenExpiredDate = new Date(expiresAt);
//   const tokenInAWeek = new Date(Date.now() + (1000 * 60 * 60 * 24 * 7));
//   // TODO recheck this validation
//   console.log('expiresAt', expiresAt)
//   if (expiresAt && tokenExpiredDate > tokenInAWeek) {
//     yield put(userAuthenticated(true));
//     return true;
//   }
//   console.log('token is expired');
//   yield put(logout());
//   let toast = {
//     message: 'Your session is expired, please log-in',
//     intent: Intent.DANGER,
//     iconName: "tick"
//   };
//   yield put(showToast(toast));
// }


function clearLocale() {
  // scrub fragment params from URL.
  if (window.history && window.history.replaceState) {
    window.history.replaceState({}, document.title, "/");
    console.log('window.history cleaned');
  } else {
    // For browsers that don't support HTML5 history API, just do it the old
    // fashioned way that leaves a trailing '#' in the URL
    window.location.hash = '#';
  }
}

// TODO figure out which query string collector is better
// function getQueryStringValue (key) {  
//   return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));  
// }  
function* watchHashParams() {
  // take hash params with OAuth2
  const access_token = helpers.getHashParams().access_token;
  const expires_in = helpers.getHashParams().expires_in;
  const expires_at = new Date(Date.now() + (expires_in * 1000));
  if ("onhashchange" in window && helpers.getHashParams().access_token) {
    yield call(clearLocale);
    yield call(setAuthToken, access_token); // save to local storage
    yield put(newToken({access_token, expires_at}));
    console.log('[token manager] access_token in store');
  }
}


export default [watchHashParams, authFlow];
