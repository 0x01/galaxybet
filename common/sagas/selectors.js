//selectors
export const getUser = (state, login) => state.app.token.auth.user;
export const getToken = (state) => state.app.accessToken;
export const getExpiresAt = (state) => state.app.expires_at;
export const getIsAuthenticated = (state) => state.app.isAuthenticated;
export const getBets = (state) => state.bets;
