import io from 'socket.io-client';
import {fork, take, select, call, put, cancel, takeLatest, actionChannel, eventChannel } from 'redux-saga/effects';
import {
  newToken, userData, bankrollData, logout, login, loginRequest, allBets, nextHash,
  userAuthenticated, initChat, newMessage, sendMessage, depositAddress,
  showToast, placeBet, betSuccess
} from '../actions';
import config from '../mpConfig';

const authPayload = {
  app_id: config.app_id,
  access_token: localStorage.getItem('access_token'),
  subscriptions: ['CHAT', 'DEPOSITS']
};

function connect() {
  const socket = io(config.chat_uri);
  return new Promise(resolve => {
    socket.on('connect', () => {
      resolve(socket);
    });
  });
}

function subscribe(socket) {
  return eventChannel(emit => {
    //authorizing socket
    socket.emit('auth', authPayload, (err, authResponse) => {
      console.log('Connected to socketpot');
      if (err) console.log('Error in auth response:', err);
      console.log('authresp', authResponse);
      emit(initChat(authResponse));
      console.log('Successful auth response:', authResponse);
    });
    // send actions on socket events
    socket.on('new_message', (payload) => {
      console.log('on new message payload', payload)
      emit(newMessage(payload));
    });
    socket.on('client_error', (message) => {
      console.log('Client Error:', {message});
      emit(showToast({message}));
    });
    socket.on('unconfirmed_balance_change', (payload) => {
      console.log('[unconfirmed_balance_change]', payload);
    });
    socket.on('balance_change', (payload) => {
      console.log('[balance_change]', payload);
    });
    return () => {};
  });
}

function* read(socket) {
  const channel = yield call(subscribe, socket);
  while (true) {
    let action = yield take(channel);
    console.log('action?:', action)
    yield put(action);
  }
}

function* write(socket) {
  while (true) {
    // const { payload } = yield take(sendMessage);
    socket.on('open', (err, msg) => {
      let data = JSON.stringify({
        data: {
          user_id: this.props.user._id
        },
        meta: { event: 'meeting:user:connect' }
      });
      socket.send(data);
      if (err) console.warn('Error submitting message: ' + err);
    });
  }
}

function* handleIO(socket) {
  yield fork(read, socket);
  yield fork(write, socket);
}

function* socketFlow() {
  while (true) {
    // mock will be never fired, should be changed to confid flow
    let { payload } = yield take(initChat);
    const socket = yield call(connect);
    const task = yield fork(handleIO, socket);

    yield take(logout);
    yield cancel(task);
    socket.emit('logout');
    console.log('flow logout fired');
  }
}

export default [socketFlow];
