import { connect } from 'react-redux';
import 'whatwg-fetch';
import config from '../mpConfig';

import { getAuthToken } from './localStorage';

const API_ROOT = config.mp_api_uri;

// Fetches an API response, the result JSON.
function callApi(endpoint, params, method) {
  const t = localStorage.getItem('state');
  const parseToken = JSON.parse(t);
  const token = parseToken.app.access_token;
  if (!token) {
    // TODO toast or modal with no accesstoken please login
    return new Error('Must have accessToken set to call MoneyPot API');
  }

  // TODO looks bad, but do it job
  const url = API_ROOT + endpoint;
  const urlToken = `${url}?access_token=${token}`;
  var fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? token ? urlToken : url : endpoint;

  if (params) {
    const esc = encodeURIComponent;
    const query = Object.keys(params)
          .map(k => `${esc(k)}=${esc(params[k])}`)
          .join('&');

    var fullUrl = `${url}?access_token=${token}&${query}`;
  }

  const options = {
    method: method || 'GET',
    headers: endpoint === '/v1/bets/simple-dice'
      ? { "Content-Type" : "application/json" }
        : {}
  };

  console.log('asoentuhaeosntuh!!!!', options)
  return fetch(fullUrl, options)
    .then(response =>
        response.json().then(json => ({ json, response }))
         ).then(({ json, response }) => {
           if (!response.ok) return Promise.reject(json);
           console.log('api response', json);
           return json;
         })
    .then(
      response => ({response}),
      error => ({error: error.message || 'Something bad happened'})
    );

}

function placeBet(endpoint, params, method) {
  const t = localStorage.getItem('state');
  const parseToken = JSON.parse(t);
  const token = parseToken.app.access_token;
  if (!token)
    throw new Error('Must have accessToken set to call MoneyPot API');

  // TODO looks bad, but do it job
  const url = API_ROOT + endpoint;
  // const urlToken = `${url}?access_token=${token}`;
  // var fullUrl = (endpoint.indexOf(API_ROOT) === -1) ? token ? urlToken : url : endpoint;


  let fullUrl = `${url}?access_token=${token}`;
  console.log('params', params)

  const options = {
    method: 'POST',
    headers: { "Content-Type" : "application/json" },
    body: JSON.stringify({
      client_seed: 0,
      cond: params.cond,
      hash: params.hash,
      wager: params.wager,
      payout: params.payout,
      target: params.target
    })
  }
  return fetch(fullUrl, options)
    .then(response =>
          response.json().then(json => ({ json, response }))
         ).then(({ json, response }) => {
           if (!response.ok) return Promise.reject(json);
           console.log('api response', json);
           return json;
         })
    .then(
      response => ({response}),
      error => ({error: error.message || 'Something bad happened'})
    );

}
// api services
export const fetchAllBets  = () => callApi('/v1/list-bets', {
  app_id: config.app_id, limit: config.bet_buffer_size
});

export const simpleDice = (params) => placeBet('/v1/bets/simple-dice', params, "POST");

export const fetchBankroll = () => callApi(`/v1/bankroll`);
export const fetchToken    = () => callApi('/v1/token');
export const fetchAdress   = () => callApi('/v1/deposit-address');
export const fetchHashes   = () => callApi('/v1/hashes');

// post
export const postHashes    = () => callApi('/v1/hashes', undefined, "POST");
export const postTip       = () => callApi('/v1/tip', undefined, "POST");

// bodyParams is an object:
// - wager: Int in satoshis
// - client_seed: Int in range [0, 0^32)
// - hash: BetHash
// - cond: '<' | '>'
// - number: Int in range [0, 99.99] that cond applies to
// - payout: how many satoshis to pay out total on win (wager * multiplier)
