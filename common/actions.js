import { createAction } from 'redux-act';

// ==== app ====
// auth
export const loginRequest      = createAction('login request');
export const newToken          = createAction('new token');
export const userAuthenticated = createAction('user authenticated');
export const logout            = createAction('logout success');

// API data
export const userData          = createAction('user data from api');
export const bankrollData      = createAction('bankroll data from api');
export const nextHash          = createAction('next hash');
export const depositAddress    = createAction('deposit address');

// toasts
export const showToast         = createAction('show toast');
export const clearToast        = createAction('clear toast');

// ==== bets ====
export const allBets           = createAction('all bets received');
export const updateWager       = createAction('update wager');
export const updateMultiplier  = createAction('update multiplier');
export const updateCondition   = createAction('update condition');
export const updateTarget      = createAction('update target');
export const placeBet          = createAction('place new bet');
export const betSuccess        = createAction('new bet success');

// ==== chat ====
export const toggleChat        = createAction('toggle chat');
export const changeChannel     = createAction('change channel');
export const sendMessage       = createAction('send message');
export const newMessage        = createAction('new message');


// need a good name

export const login = createAction('login success');
export const authCanceled = createAction('auth canceled');
export const initChat = createAction('initChat');
