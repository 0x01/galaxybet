import React from 'react';
import Navigation from '../Navigation';
import s from './Header.styl';

class Header extends React.Component {

  render() {
    return (
      <header className={`header`} ref={node => (this.root = node)}>
        <Navigation />
      </header>
    );
  }

}

export default Header;
