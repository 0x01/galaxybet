import React           from 'react';
import { connect }     from 'react-redux';
import config          from '../../common/mpConfig';
/* import NumberEasing    from 'react-number-easing';*/

/* <NumberEasing value={2} speed={200} />*/
/* import {  } from '../../common/actions';*/
import './BalanceBox.styl';


@connect(state => ({
  balance: state.app.user.auth.user.balance,
}))
export default class BalanceBox extends React.Component {

  // TODO simplify this into the one function
  _openWithdrawPopup() {
    let windowUrl = config.mp_browser_uri + '/dialog/withdraw?app_id=' + config.app_id;
    let windowName = 'manage-auth';
    let windowOpts = [
      'width=420',
      'height=350',
      'left=100',
      'top=100'
    ].join(',');
    let windowRef = window.open(windowUrl, windowName, windowOpts);
    windowRef.focus();
    return false;
  }

  _openDepositPopup() {
    let windowUrl = config.mp_browser_uri + '/dialog/deposit?app_id=' + config.app_id;
    let windowName = 'manage-auth';
    let windowOpts = [
      'width=420',
      'height=350',
      'left=100',
      'top=100'
    ].join(',');
    let windowRef = window.open(windowUrl, windowName, windowOpts);
    windowRef.focus();
    return false;
  }

  render() {
    const { balance } = this.props;
    let sat = balance / 100000000;
    const balanceNumber = sat ? sat.toFixed(8) : '0.00000000';
    return (
      <div className="balance-wrapper">
        <div className="balance">
          <span className="balance-icon">
            <img src="https://cdn1.iconfinder.com/data/icons/virtual-money/500/2_Baht_bit_bitcoin_buy_cash_coin_currency_money_e-gold-512.png" />
          </span>
          <span className="balance-number">
            {balanceNumber}
          </span>
          <span className="balance-currency">BTC</span>
        </div>
      </div>
    );
  }
}
