import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as Blueprint from '@blueprintjs/core';
import { showToast } from '../../common/actions'; // THE ABOVE FILE

@connect((state) => ({
  toastQueue: state.app.toast,
}), { showToast })

export default class AppWrapper extends Component {

  componentDidUpdate(prevProps, prevState) {
    console.log('props from toast', this.props);
    if (this.props.toastQueue.length > 0) {
      this.props.toastQueue.forEach((toast) => {
        this.toaster.show(toast);
      });
      /* this.props.clearToast();*/
    }
  }

  render() {
    return (
      <div>
        <Blueprint.Toaster position={ Blueprint.Position.TOP_CENTER } ref={ el => this.toaster = el } />
        { this.props.children }
      </div>
    );
  }
}
