import React                      from 'react';
import { connect }                from 'react-redux';
import { Button, Intent, Dialog } from '@blueprintjs/core';

export default class Support extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  toggleDialog = () => this.setState({ isOpen: !this.state.isOpen });

  render() {
    return (
      <div className="support-button-wrapper">
        <Button className="pt-minimal" text="Support" onClick={this.toggleDialog} />
        <Dialog
          isOpen={this.state.isOpen}
          onClose={this.toggleDialog}
          title="TwiceDice 24/7 SUPPORT"
        >
          <div className="pt-dialog-body">
            <div className="pt-ui-text">
              <h5>
                Ask our live chat for help first!
              </h5>
              The galaxybet chat community is always willing to offer immediate help to other players. If you are unable to find a solution please proceed with a support ticket.
            </div>
            <div className="pt-select pt-large pt-fill">
              <select>
                <option value='1'>Choose a ticket header...</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
                <option value="4">Four</option>
              </select>
            </div>
            <div className="support-inputs">
              <input className="pt-input pt-large" type="text" placeholder="Username" dir="auto" />
              <input className="pt-input pt-large" type="text" placeholder="Email" dir="auto" />
            </div>
            <textarea className="pt-input support-textarea pt-fill" placeholder="Tell us about your issue" dir="auto" />
          </div>

          <div className="pt-dialog-footer">
            <Button
              className="pt-large pt-fill"
              intent={Intent.PRIMARY}
              onClick={this.toggleDialog}
              text="SEND"
            />
          </div>
        </Dialog>
      </div>
    );
  }
}
