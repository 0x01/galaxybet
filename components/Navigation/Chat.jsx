import React                from 'react';
import { connect }          from 'react-redux';
import { Button }           from '@blueprintjs/core';
import { toggleChat }       from '../../common/actions';
import './Navigation.styl';


@connect(state => ({
  isOpen: state.chat.isOpen // State
}), { toggleChat })      // Actions

export default class Chat extends React.Component {

  handleClick = () => this.props.toggleChat(!this.props.isOpen);
  render() {
    return (
      <div>
        <Button iconName="chat" className="pt-minimal" onClick={this.handleClick} />
      </div>
    );
  }
}
