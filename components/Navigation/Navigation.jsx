import React               from 'react';
import { Button, Tooltip } from "@blueprintjs/core";
import Link                from '../Link';
import Support             from './Support.jsx';
import Deposit             from './Deposit.jsx';
import Withdraw             from './Withdraw.jsx';
import Chat                from './Chat.jsx';
import Login                from './Login.jsx';
import './Navigation.styl';


export default class Navigation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  toggleDialog = () => this.setState({ isOpen: !this.state.isOpen });

  render() {
    return (
      <nav className="pt-navbar" ref={node => (this.root = node)}>
        <div className="pt-navbar-group pt-align-left">
          <Link className="app-logo" to="/">
            <div className="pt-navbar-heading">App</div>
          </Link>
        </div>
        <div className="pt-navbar-group pt-align-left">
          <Tooltip tooltipClassName="pt-dark" content="Notifications" useSmartPositioning={true} >
            <Button className="pt-minimal" iconName="notifications" />
          </Tooltip>
          <span className="pt-navbar-divider" />
          <Button className="pt-minimal" iconName="cog" />
          <span className="pt-navbar-divider" />
          <Chat />
        </div>
        <div className="pt-navbar-group pt-align-right">
          <Deposit />
          <Withdraw />
          <Support className="pt-align-right" />
          <span className="pt-navbar-divider" />
          <Login />
        </div>
      </nav>
    );
  }
}
