import React from 'react';
import config from '../../common/mpConfig';

export default class Deposit extends React.Component {

  _openDepositPopup() {
    let windowUrl = config.mp_browser_uri + '/dialog/deposit?app_id=' + config.app_id;
    let windowName = 'manage-auth';
    let windowOpts = [
      'width=420',
      'height=350',
      'left=100',
      'top=100'
    ].join(',');
    let windowRef = window.open(windowUrl, windowName, windowOpts);
    windowRef.focus();
    return false;
  }
  render() {
    return (
      <div className="deposit-button-wrapper">
        <div className="deposit" onClick={this._openDepositPopup}>DEPOSIT</div>
      </div>
    )
  }
}
