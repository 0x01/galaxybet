import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Button, Dialog, Tooltip } from "@blueprintjs/core";
import { logout, loginRequest, showToast } from '../../common/actions';


@connect(state => ({
  user: state.app.user.auth.user,
  isAuth: state.app.isAuthenticated
}), { logout, loginRequest, showToast })      // Actions
export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  toggleDialog = () => this.setState({ isOpen: !this.state.isOpen });

  handleLogin = () => {
    window.open("https://www.moneypot.com/oauth/authorize?app_id=2744&redirect_uri=http://localhost:3000", "_self").focus();
    this.props.loginRequest();
  }

  handleLogout = () => {
    this.props.showToast({message: 'You logged out, see you soon.'});
    this.props.logout();
  }
  
  render() {
    let headerName = null;
    let button = null;

    if (this.props.isAuth && this.props.user.uname) {
      button = <Button onClick={this.handleLogout} className="pt-minimal" iconName="log-out" />;
      headerName = <div className="login-uname">{this.props.user.uname}</div>;
    } else {
      button = <Button onClick={this.toggleDialog} className="pt-minimal" iconName="log-in" />;
      headerName = <div className="login-uname">Greetings, friend</div>;
    }

    return (
      <div className="login-button-wrapper">
        <div className="login-uname">
          {headerName}
        </div>
        {button}
        <Dialog
          isOpen={this.state.isOpen}
          onClose={this.toggleDialog}
          title="Login info"
        >
          <div className="pt-dialog-body">
            <span>Login with MoneyPot</span>
          </div>
          <div className="pt-dialog-footer">
            <Button onClick={this.handleLogin} className="login-button pt-button pt-icon-log-in pt-fill">
            </Button>
          </div>
        </Dialog>
      </div>
    );
  }
}

/* <Tooltip tooltipClassName="pt-dark" content="Login" useSmartPositioning={true} >
 * </Tooltip>*/
