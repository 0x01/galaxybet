import React           from 'react';
import moment          from 'moment';
import * as helpers    from '../../common/helpers';
import { connect }     from 'react-redux';
import { toggleChat, changeChannel, sendMessage } from '../../common/actions';
import './BetsTable.styl';

@connect(state => ({
  bets: state.bets.all_bets,    // State
}))      // Actions
export default class BetsTable extends React.Component {

  render() {
    const { bets } = this.props;

    return (
      <div className="bets-table-wrapper">
        <table className="pt-table pt-striped pt-interactive">
          <thead>
            <th>BET ID</th>
            <th>USER</th>
            <th>TIME</th>
            <th>BET</th>
            <th>PAYOUT</th>
            <th>GAME</th>
            <th>ROLL</th>
            <th>PROFIT</th>
          </thead>
          <tbody>
            {bets.map(bet =>
              <tr key={bet.id}>
                <td>{bet.id}</td>
                <td>{bet.uname}</td>
                <td>{moment(bet.created_a).format('h:mm')}</td>
                <td>{(bet.wager / 100000000).toFixed(8)}</td>
                <td>{bet.target}</td>
                <td>{bet.target}</td>
                <td>{bet.outcome}</td>
                <td style={{color: bet.profit >= 0 ? 'green' : 'red' }}>
                  {
                    bet.profit >= 0
                    ? `+${((bet.profit + bet.wager) / 100000000).toFixed(8)}`
                    : (bet.profit / 100000000).toFixed(8)
                  }
                </td>
              </tr>
             )}
          </tbody>
        </table>
      </div>
    );
  }
}
