import React           from 'react';
import { connect }     from 'react-redux';
import config          from '../../common/mpConfig';
import Wager           from './Wager.jsx';
import Multiplier      from './Multiplier.jsx';
import RollToWin       from './RollToWin.jsx';
import Chance          from './Chance.jsx';
import BetButton       from './BetButton.jsx';
import Profit          from './Profit.jsx';
import { updateWager, updateMultiplier, updateCondition, updateTarget, placeBet } from '../../common/actions';
import './BetBox.styl';

@connect(state => ({
  bets: state.bets,
  isAuthenticated: state.app.isAuthenticated,
  balance: state.app.user.auth.user.balance
}), { updateWager, updateMultiplier, updateCondition, updateTarget, placeBet })

export default class BetBox extends React.Component {

  componentWillMount() {
    if (!this.props.isAuthenticated) {
      this.props.updateMultiplier(parseFloat(2.000));
      this.props.updateCondition('<');
      this.props.updateTarget(parseFloat(49.50));
    }
  }

  _updateWager = (value) => this.props.updateWager(value);
  _updateMultiplier = (value) => this.props.updateMultiplier(value);
  _updateMultiplierWithChance = (value) => this.props.updateMultiplier(value);
  _updateCondition = (value) => this.props.updateCondition(value);
  _updateTarget = (value) => this.props.updateTarget(value); 

  _placeBet = () => this.props.placeBet();

  render() {
    const { bets } = this.props;
    return (
      <div className="bet-box-wrapper">
        <div className="bet-box-tab">
          <button className="bet-box-tab-button">Manual</button>
          <button className="bet-box-tab-button">Automated</button>
        </div>
        <div className="bet-box-content">
          <div className="bet-box-content-upper">
            <Wager
              updateWager={this._updateWager}
              wager={this.props.bets.wager}
              isAuthenticated={this.props.isAuthenticated}
              balance={this.props.balance}
            />
            <Profit
              wager={this.props.bets.wager}
              multiplier={this.props.bets.multiplier}
            />
          </div>

          <div className="bet-box-content-lower">
            <RollToWin
              updateCondition={this._updateCondition}
              updateTarget={this._updateTarget}
              multiplier={this.props.bets.multiplier}
              condition={this.props.bets.condition}
              target={this.props.bets.target}
            />
            <Multiplier
              updateMultiplier={this._updateMultiplier}
              updateTarget={this._updateTarget}
              multiplier={this.props.bets.multiplier}
            />
            <Chance
              updateMultiplierWithChance={this._updateMultiplierWithChance}
              multiplier={this.props.bets.multiplier}
              updateTarget={this._updateTarget}
            />
            <BetButton
              placeBet={this._placeBet}
            />
          </div>
        </div>
      </div>
    );
  }
}
