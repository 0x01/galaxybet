import React from 'react';

export default class Profit extends React.PureComponent {
  render() {
    let profit = this.props.wager * (this.props.multiplier - 1);
    let number = profit ? profit.toFixed(8) : '0.00000000';
    return (
      <div className="profit-wrapper">
      <span className="profit-title">PROFIT ON WIN</span>
        <div className="profit-content">
          <div className="profit-number">
            {number}
          </div>
        </div>
      </div>
    );
  }
}
