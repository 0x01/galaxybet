import React from 'react';
import NumericInput from 'react-numeric-input';

export default class Wager extends React.Component {

  _onWagerChange = (e) => {
    if (e.target.value === null) return 0.00000000;
    const wager = e.target.value;
    this.props.updateWager(wager);
  }

  _onHalveWager = () => {
    let wager = this.props.wager / 2;
    this.props.updateWager(wager);
  }

  _onDoubleWager = () => {
    let wager = this.props.wager * 2;
    this.props.updateWager(wager);
  }

  _onMaxWager = () => {
    // If user is logged in, use their balance as max wager
    console.log('thisprops', this.props);
    let wager = this.props.balance;
    this.props.updateWager(wager);
  }

  render() {
    return (
      <div className="wager-wrapper">
        <div className="wager-title">WAGER</div>
        <div className="wager-content" >
          <input
            className="wager-input"
            value={this.props.wager}
            onChange={this._onWagerChange}
          />
          <button onClick={this._onDoubleWager}>x2</button>
          <button onClick={this._onHalveWager}>/2</button>
          <button onClick={this._onMaxWager}>MAX</button>
        </div>
      </div>
    );
  }
}

/* <NumericInput
 *   className="wager-input"
 *   value={this.props.wager}
 *   max={5}
 *   min={0}
 *   step={0.00000001}
 *   precision={8}
 *   onChange={this._onWagerChange}
 * />*/
