import React from 'react';
import config from '../../common/mpConfig';

export default class Wager extends React.Component {

  _onMultiplierChange = (e) => {
    const multiplier = parseInt(Number(e.target.value).toPrecision(4));
    console.log('multipiler', multiplier);
    this.props.updateMultiplier(multiplier);
    this._multiplierToTarget(multiplier);
  }

  _multiplierToTarget = (multiplier) => {
    // multiplier shoud be a number
    const n = 1.0 - config.house_edge; // n is 0.99 when house edge is 1%
    const chance = (n / multiplier) * 100;
    this.props.updateTarget(chance);
  }

  render() {
    return (
      <div className="multiplier-wrapper">
        <span className="multiplier-title box-title">PAYOUT</span>
        <div className="multiplier-content">
          <input
            className="multiplier-input box-input"
            value={this.props.multiplier}
            onChange={this._onMultiplierChange}
          />
        </div>
      </div>
    );
  }
}

/* <NumericInput
 *   className="multiplier-input box-input"
 *   value={this.props.multiplier}
 *   max={9900}
 *   min={1.0102}
 *   precision={4}
 *   step={0.1}
 *   onChange={this._onMultiplierChange}
 *   form={this._form}
 * />*/
