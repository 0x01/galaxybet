import React from 'react';
import config from '../../common/mpConfig';

export default class RollToWin extends React.PureComponent {

  _reverseNumber = (cond) => {
    const target = 99.99 - this.props.target;
    this.props.updateTarget(target);
  }

  _toggleCondition = () => {
    const cond = this.props.condition === '<' ? '>' : '<';
    this.props.updateCondition(cond);
    this._reverseNumber(cond);
  }

  render() {
    const word = this.props.condition === '<' ? 'Under' : 'Over';
    const phrase = `Roll ${word}`;

    const target = Number(this.props.target).toFixed(2);
    return (
      <div className="rtw-wrapper" onClick={this._toggleCondition}>
        <span className="rtw-condition-title box-title">
          {phrase}
        </span>
        <div className="rtw-number" >
          {this.props.condition} {target}
        </div>
      </div>
    );
  }
}
