import React from 'react';
import NumericInput from 'react-numeric-input';
import config from '../../common/mpConfig'

export default class Chance extends React.PureComponent {

  _onChanceChange = (e) => {
    let n = 1.0 - config.house_edge;
    let updatedMultiplier = (n / e.target.value) * 100;
    this.props.updateMultiplierWithChance(updatedMultiplier);
  }

  _multiplierToChance = (multiplier) => {
    // multiplier shoud be a number
    let n = 1.0 - config.house_edge; // n is 0.99 when house edge is 1%
    let chance = (n / multiplier) * 100;
    this.props.updateTarget(chance);
    return chance;
  }


  render() {
    const chance = this._multiplierToChance(this.props.multiplier).toFixed(2);
    return (
      <div className="chance-wrapper">
        <span className="chance-title box-title">CHANCE</span>
        <span className="chance-number">
          <input
            className="chance-input box-input"
            value={chance}
            onChange={this._onChanceChange}
          />
        </span>
      </div>
    );
  }
}
