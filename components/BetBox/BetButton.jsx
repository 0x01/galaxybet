import React from 'react';

export default class ButButton extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      waitingForServer: false
    }
  }
  _placeBet = () => {
    this.props.placeBet();
  }
  render() {
    return (
      <div className="bet-button-wrapper" onClick={this._placeBet}>
        <button type="button" className="bet-button">ROLL DICE</button>
      </div>
    );
  }
}
