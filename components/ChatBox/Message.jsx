import React from 'react';

export default class Message extends React.Component {
  render() {
    const now = new Date(this.props.created_at);
    const hhmm = now.toISOString().substr(11, 5); 
    return (
      <div className="message">
        <strong className="message-uname">{this.props.uname}</strong> 
        <span className="message-time">{hhmm}</span> 
        <span className="message-text">{this.props.text}</span>
      </div>
    );
  }
}
