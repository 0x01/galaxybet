import React   from 'react';
import Message from './Message.jsx';
import $       from 'jquery';

export default class MessageList extends React.Component {

  componentDidMount() {
    setTimeout(() =>
      $(this.messageListEl).scrollTop(this.messageListEl.scrollHeight), 1000);
  }
  componentDidUpdate() {
    this.scrollToBottom(this.messageListEl);
  }

  /* scroll the chat to bottom on mount and update */
  scrollToBottom = (el) => {
    const shouldScroll = () => {
      const distanceFromBottom = el.scrollHeight - ($(el).scrollTop() + $(el).innerHeight());
      return distanceFromBottom <= 500;
    };

    if (shouldScroll()) $(el).scrollTop(el.scrollHeight);
  }
  render() {
    return (
      <div className="chat-channel" ref={el => this.messageListEl = el}>
        {this.props.messages.filter(message => message.channel === this.props.channel).map(message =>
          <Message
            created_at={message.created_at}
            role={message.user.role}
            uname={message.user.uname}
            text={message.text}
            key={message.id}
          />,
         )}
      </div>
    );
  }

}

//TODO need to change scrollbar to make it more beautiful, but applying this cause to changing scrollToBottom function

/* import { Scrollbars }  from 'react-custom-scrollbars'*/
/* <Scrollbars
 *   renderThumbHorizontal={props => <div {...props} style={{ display: 'none' }} />}
 *   renderThumbVertical={props => <div {...props} className="thumb-vertical" />}
 *   renderTrackVertical={props => <div {...props} className="track-vertical" />}
 *   autoHeight
 *   autoHeightMin={1000}>
 * </Scrollbars>*/
