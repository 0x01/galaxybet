import React    from 'react';
import Editor from 'draft-js-plugins-editor';
import createCounterPlugin from 'draft-js-counter-plugin';
import { EditorState } from 'draft-js';
import LanguageButton from '../Button/LanguageButton';

const counterPlugin = createCounterPlugin();
const plugins = [counterPlugin];
const { CharCounter } = counterPlugin;

export default class MessageForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editorState: EditorState.createEmpty(),
      selected: '',
    };
  }

  onChange = (editorState) => this.setState({editorState});
  focus = () => this.editor.focus();
  /* logState = () => console.log(this.state.editorState.toJS());*/

  handleReturn = (e) => {
    if (!e.shiftKey) {
      // submit message
      const { channel } = this.props;
      /* console.log('return channel', channel)*/
      this.props.appendChatMessage(this.state.editorState.getCurrentContent().getPlainText(), channel);
      // clear
      this.setState({
        editorState: EditorState.moveFocusToEnd(EditorState.createEmpty()),
      });
      return 'handled';
    }
    return 'not-handled';
  }

  handleChannelChange = (e) => {
    this.props.appendChannel({channel: e.target.value}); 
    /* this.chan = e.target.value;*/
    /* setTimeout(() => {
     *   console.log(this.chan, this.props.channel)
     *   if (this.chan === this.props.channel) {active = {background: 'green'}}}, 0);*/
  }

  // TODO make channels more generic
  render() {
    return (
      <div className="wrapper" >
        <div className="editor-wrapper">
          <Editor
            editorState={this.state.editorState}
            onChange={this.onChange}
            ref={editor => this.editor = editor}
            handleReturn={this.handleReturn}
            placeholder="Write a message..."
            plugins={plugins}
          />
        </div>
        <div className="pt-button-group">
          <button onClick={this.handleChannelChange} className="pt-button" value="lobby" >en</button>
          <button onClick={this.handleChannelChange} className="pt-button" value="ru" >ru</button>
          <button onClick={this.handleChannelChange} className="pt-button" value="fr" >fr</button>
          <button onClick={this.handleChannelChange} className="pt-button" value="il" >il</button>
          <button onClick={this.handleChannelChange} className="pt-button" value="sp" >sp</button>
          <button onClick={this.handleChannelChange} className="pt-button" value="du" >du</button>
          <CharCounter editorState={this.state.editorState} limit={300} />
        </div>

      </div>
    );
  }
}

