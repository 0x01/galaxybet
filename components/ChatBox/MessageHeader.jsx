import React                           from 'react';
import { Button }                      from '@blueprintjs/core';

export default class MessageHeader extends React.Component {
  handleClick = () => {
    this.props.chatIsOpen(false);
  }
  render() {
    return (
      <div className="chat-header-wrapper">
        <Button iconName="cross" className="pt-minimal" onClick={this.handleClick} />
        <div className="">online: {Object.keys(this.props.userlist).length}</div>
        <Button iconName="cog" className="pt-minimal" />
      </div>
    );
  }
}
