import React           from 'react';
import { connect }     from 'react-redux';
import MessageList     from './MessageList.jsx';
import MessageForm     from './MessageForm.jsx';
import MessageHeader   from './MessageHeader.jsx';
import { toggleChat, changeChannel, sendMessage } from '../../common/actions';
import './ChatBox.styl';

const openedMenu = {
  transform: 'translateX(0px)',
  visibility: 'visible',
};
const closedMenu = {
  transform: "translateX(370px)",
  visibility: 'hidden',
};


@connect(state => ({
  chat: state.chat,                                  // State
}), { toggleChat, changeChannel, sendMessage })      // Actions
export default class ChatBox extends React.Component {

  /* add messages on submit see [MessageForm]*/
  appendChatMessage = (text, channel) => this.props.sendMessage({text, channel});
  chatIsOpen = (val) => this.props.toggleChat(val);
  appendChannel = (val) => this.props.changeChannel(val);

  render() {
    const { chat } = this.props;
    return (
      <div className="chat-wrapper" style={chat.isOpen ? {...openedMenu} : {...closedMenu}}>
        <div className="chat">
          <div className="chat-header">
            <MessageHeader
              chatIsOpen={this.chatIsOpen}
              userlist={chat.userlist} />
          </div>
          <div className="chat-body">
            <MessageList
              messages={chat.messages}
              channel={chat.channel} />
          </div>
          <div className="chat-footer">
            <MessageForm
              appendChatMessage={this.appendChatMessage}
              appendChannel={this.appendChannel}
              channel={chat.channel} />
          </div>
        </div>
      </div>
    );
  }
}
