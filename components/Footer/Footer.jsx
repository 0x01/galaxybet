import React from 'react';
import Link from '../Link';
import './Footer.styl';

function Footer() {
  return (
    <footer className="footer">
      <div className="wrapper">
        <div className="internal-links">
          <Link to="/privacy">Privacy & Terms</Link>
          <Link to="/faq">FAQ</Link>
        </div>
        <div className="copyrights">© 2017 All Rights Reserved.</div>
      </div>
    </footer>
  );
}

export default Footer;
