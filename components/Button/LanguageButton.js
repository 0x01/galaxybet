import React, { PropTypes } from 'react';
import cx from 'classnames';

class Button extends React.Component {

  static propTypes = {
    component: PropTypes.oneOf([
      PropTypes.string,
      PropTypes.element,
      PropTypes.func,
    ]),
    text: PropTypes.string,
    className: PropTypes.string,
    children: PropTypes.node,
  };

  render() {
    const { component, className, children, ...other } = this.props;
    return React.createElement(
      component,
      {
        ref: node => (this.root = node),
        className: cx(
          'pt-button',
          className
        ),
        ...other,
      },
      children
    );
  }

}

export default Button;
