import React, { PropTypes } from 'react';
import cx from 'classnames';
import Link from '../Link';

class Button extends React.Component {

  static propTypes = {
    component: PropTypes.oneOf([
      PropTypes.string,
      PropTypes.element,
      PropTypes.func,
    ]),
    icon: PropTypes.oneOf([,,,'','person','more','info', 'link', 'pulse', 'ninja', 'moon', 'glass', 'flash', 'contrast', 'chat', 'caret-down', 'cog', 'notifications', 'user', 'cross']),
    size: PropTypes.oneOf(['minimal', 'large', 'fill']),
    to: PropTypes.oneOf([PropTypes.string, PropTypes.object]),
    href: PropTypes.string,
    text: PropTypes.string,
    className: PropTypes.string,
    primary: PropTypes.bool,
    accent: PropTypes.bool,
    disabled: PropTypes.bool,
    children: PropTypes.node,
  };

  render() {
    const { component, icon, text, size, className, to, href,
      primary, accent, disabled, children, ...other } = this.props;
    return React.createElement(
      component || (to ? Link : (href ? 'a' : 'button')), // eslint-disable-line no-nested-ternary
      {

        ref: node => (this.root = node),
        className: cx(
          'pt-button',
          icon && `pt-icon-${icon}`,
          size && `pt-${size}`,
          {
            'pt-intent-primary': primary,
            'pt-intent-accent': accent,
            'pt-disabled': disabled,

          },
          className
        ),
        to,
        href,
        ...other,
      },
      children
    );
  }

}

export default Button;
